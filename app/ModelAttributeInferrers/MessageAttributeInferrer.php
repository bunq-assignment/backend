<?php

namespace App\ModelAttributeInferrers;

class MessageAttributeInferrer extends AttributeInferrer {

    protected function inferForInsert($model) {
        $model->user_id = auth()->user()->id;
        return $model;
    }

    protected function inferForUpdate($model) {
        $model->user_id = auth()->user()->id;
        return $model;
    }

}