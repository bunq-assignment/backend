<?php

namespace App\ModelAttributeInferrers;

class AttributeInferrer {

    public function inferFor($operation, $model) {
        if ($operation == 'insert') {
            $this->inferForInsert($model);
        } else if ($operation == 'update') {
            $this->inferForUpdate($model);
        }
        
    }

    protected function inferForInsert($model) {
        // $model->attr = _value_
    }

    protected function inferForUpdate($model) {
        // $model->attr = _value_
    }

}