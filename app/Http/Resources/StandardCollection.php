<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * A custom collection class that serializes a collection of eloquent models and their included relationships into a response that conform to the JSONAPI specification.
 * read more at: https://jsonapi.org/format/
 */
class StandardCollection extends ResourceCollection
{
    public $collects = StandardResource::class;

    public $included = [];
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request)
    {
        foreach ($this->resource as $res) {
            foreach ($res->included as $key => $inc) {
                $this->included[$key] = $inc;
            }
        }
        [$keys, $models] = array_divide($this->included);
        return [
            'included' => $models
        ];
    }
}
