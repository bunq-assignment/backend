<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * A custom resource class that serializes an eloquent model and its included relationships into a response that conform to the JSONAPI specification.
 * read more at: https://jsonapi.org/format/
 */
class StandardResource extends JsonResource
{
    public $relationships = [];
    public $included = [];
    public $attributes = [];

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->collectResourceAttributes();
        $this->collectResourceRelationships();
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->resource->getTable(),
            'id' => $this->resource->id,
            'attributes' => $this->attributes,
            'relationships' => $this->relationships
        ];
    }

    /**
     * Helper function to retrieve a key-value array of the resources non-hidden attributes .
     *
     * @return array
     */
    protected function collectResourceAttributes() 
    {
        $this->attributes = array_except($this->resource->getAttributes(), $this->resource->getHidden());
        $this->attributes = array_except($this->attributes, 'id');
    }

    /**
     * Helper function to recursively retrieve all included related models with the resource.
     *
     * @return array
     */
    protected function collectResourceRelationships()
    {
        foreach ($this->resource->getRelations() as $rel => $value) {
            // discard intermediate pivot relationships
            if ($rel === 'pivot') {
                continue;
            }

            $data = [];

            if ($value instanceof \Illuminate\Database\Eloquent\Collection) {
                foreach ($value as $obj) {
                    $data[] = ['id' => $obj->id, 'type' => $obj->getTable()];
                    $res = new StandardResource($obj);
                    // propagate included related models and create unique keys to prevent duplication
                    $this->included[$obj->getTable() . ' ' . $obj->id] = $res;
                    foreach ($res->included as $key => $inc) {
                        $this->included[$key] = $inc;
                    }
                }
            } else {
                $data = ['id' => $value->id, 'type' => $value->getTable()];
                $res = new StandardResource($value);
                // propagate included related models and create unique keys to prevent duplication
                $this->included[$value->getTable() . ' ' . $value->id] = $res;
                foreach ($res->included as $key => $inc) {
                    $this->included[$key] = $inc;
                }
            }

            $this->relationships[$rel] = ['data' => $data];
        }
    }

    public function with($request)
    {
        [$keys, $models] = array_divide($this->included);
        return [
            'included' => $models
        ];
    }
}
