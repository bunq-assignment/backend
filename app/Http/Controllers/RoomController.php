<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use App\ModelAttributeInferrers\MessageAttributeInferrer;

class RoomController extends JsonApiController
{
    public function __construct() {
        $this->model = Room::class;
    }

    /**
     * @overridden
     * Validate store request.
     * 
     * @return void
     */
    protected function validateStore(Request $request) {
        $request->validate([
            'data.attributes.title' => 'bail|required|string|max:128',
        ]);
    }

    /**
     * @overridden
     * Validate update request.
     * 
     * @return void
     */
    protected function validateUpdate(Request $request) {
        $request->validate([
            'data.attributes.title' => 'bail|required|string|max:128',
        ]);
    }

    /**
     * @overridden
     * Defines filters that are forced on a listing request.
     *
     * @return array<any>
     */
    protected function getForcedFilters() {
        return [
            'users.id' => 'eq:' . auth()->user()->id
        ];
    }
}
