<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\ModelAttributeInferrers\MessageAttributeInferrer;

class MessageController extends JsonApiController
{
    public function __construct() {
        $this->model = Message::class;
        $this->attributeInferrer = new MessageAttributeInferrer;
    }

    /**
     * @overridden
     * Validate store request.
     * 
     * @return void
     */
    protected function validateStore(Request $request) {
        $request->validate([
            'data.attributes.content' => 'bail|required|string|max:4096',
            'data.attributes.room_id' => 'bail|required|integer|min:1|exists:rooms,id'
        ]);
    }

    /**
     * @overridden
     * Validate update request.
     * 
     * @return void
     */
    protected function validateUpdate(Request $request) {
        $request->validate([
            'data.attributes.content' => 'bail|required|string|max:4096',
            'data.attributes.room_id' => 'bail|required|integer|min:1|exists:rooms,id'
        ]);
    }

    /**
     * @overridden
     * Defines filters that are forced on a listing request.
     *
     * @return array<any>
     */
    protected function getForcedFilters() {
        if (!request()->has('filter.room_id')) {
            return [
                'user_id' => 'eq:' . auth()->user()->id
            ];
        }
        return [];
    }
}
