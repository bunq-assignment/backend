<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\StandardCollection;

class UserController extends Controller
{
    public function index(Request $request) {
        return new StandardCollection(\App\User::all());
    }
}
