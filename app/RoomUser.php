<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RoleUser extends RoomUser
{
    protected $table = 'room_users';
}